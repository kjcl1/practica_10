package Practicas;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

public class Practica_3 extends JFrame implements ActionListener{
    
	JButton btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9, btn10, btn11, btn12, btn13, btn14, btn15, btn16;
        JTextField tfNombre;
	FlowLayout miFlowLayout;
    
	public Practica_3(){

		miFlowLayout = new FlowLayout(FlowLayout.CENTER,3,3);
		setLayout(miFlowLayout);
                tfNombre = new JTextField(15);
                this.add(tfNombre);
        
                btn1 = new JButton("7");
                btn2 = new JButton("8");
                btn3 = new JButton("9");
                btn4 = new JButton("/");
                btn5 = new JButton("4");
                btn6 = new JButton("5");
                btn7 = new JButton("6");
                btn8 = new JButton("*");
                btn9 = new JButton("1");
                btn10 = new JButton("2");
		btn11 = new JButton("3");
		btn12 = new JButton("-");
		btn13 = new JButton("=");
		btn14 = new JButton("0");
		btn15 = new JButton(".");
                btn16 = new JButton("+");
                
                add(btn1);
                add(btn2);
                add(btn3);
                add(btn4);
                add(btn5);
                add(btn6);
                add(btn7);
                add(btn8);
                add(btn9);
                add(btn10);
		add(btn11);
		add(btn12);
		add(btn13);
		add(btn14);
		add(btn15);
                add(btn16);
        
                btn1.addActionListener(this);
                btn2.addActionListener(this);
                btn3.addActionListener(this);
                btn4.addActionListener(this);
                btn5.addActionListener(this);
                btn6.addActionListener(this);
                btn7.addActionListener(this);
                btn8.addActionListener(this);
                btn9.addActionListener(this);
                btn10.addActionListener(this);
		btn11.addActionListener(this);
		btn12.addActionListener(this);
		btn13.addActionListener(this);
		btn14.addActionListener(this);
		btn15.addActionListener(this);
                btn16.addActionListener(this);

	}
    
	public static void main( String args[] ) {
        
		Practica_3 formulario = new Practica_3();
		formulario.setSize(200,200);
		formulario.setTitle("CALCULADORA");
		formulario.setDefaultCloseOperation(EXIT_ON_CLOSE);
		formulario.setVisible(true);
        
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		miFlowLayout.setHgap(miFlowLayout.getHgap() + 5);
		miFlowLayout.setVgap(miFlowLayout.getVgap() + 5);
		setLayout(miFlowLayout);
		validate();
        
	}

}
